package main

import (
	"syscall"
	"fmt"
	"flag"
	"net/smtp"
	"strings"
	"crypto/tls"
	"log"
)

func main() {
	var (
		data    syscall.Statfs_t
		from    *string
		to      *string
		pass    *string
		mail    *Mail
		server  *string
		path    *string
		maxSize *int
	)
	from = flag.String("f", "", "Email sender [Cannot be empty]")
	pass = flag.String("p", "", "Password of eail sender account [Cannot be empty]")
	to = flag.String("t", "admin@tombala.tv", "Person to email [default : admin@tombala.tv]")
	server = flag.String("MailServer", "smtp.yandex.ru:465", "Mail server to use [default: smtp.yandex.ru:465]")
	path = flag.String("path", "/", "Path to inspect [default: /]")
	maxSize = flag.Int("minSize", 10, "threshold size for alert in GB [default 10]")
	flag.Parse()
	if *from == "" {
		log.Fatalln("From cannot be empty run GoFree -h for usage details")
	}
	if *pass == "" {
		log.Fatalln("password cannot be empty run GoFree -h for usage details")
	}
	syscall.Statfs(*path, &data)
	sizeInBytes := data.Bfree * uint64(data.Bsize)
	sizeInGB := sizeInBytes / (1024 * 1024 * 1024)
	if int(sizeInGB) < *maxSize {
		fmt.Println("small")
		mail = new(Mail)
		mail.From = *from
		mail.To = *to
		mail.Password = *pass
		mail.Body = "Space is running low on mongodb database server!!"
		mail.Server = *server
		mail.Headers = map[string]string{
			"Content-Type": "Text/HTML",
			"From":         "Tombalalive System Alert" + *from,
			"To":           *to,
			"Subject":      "System Alert Low Disc Space",
		}
		data, err := mail.SendWithSSL()
		var msg interface{}
		select {
		case msg = <-data:
		case msg = <-err:
		}
		log.Println(msg)
	}
	fmt.Println(sizeInGB, "GB")
}

type Mail struct {
	To       string
	From     string
	Headers  map[string]string
	Server   string
	Password string
	Body     string
}

func (m *Mail) PrepareMessage() []byte {
	var message string
	for k, v := range m.Headers {
		message += k + ": " + v + "\r\n"
	}
	message += "\r\n" + m.Body
	return []byte(message)
}
func (m *Mail) SendWithSSL() (chan bool, chan error) {
	var (
		errC = make(chan error)
		done = make(chan bool)
	)
	go func() {
		auth := smtp.PlainAuth("", m.From, m.Password, strings.Split(m.Server, ":")[0])
		tlsConf := &tls.Config{
			InsecureSkipVerify: true,
			ServerName:         m.Server,
		}
		con, err := tls.Dial("tcp", m.Server, tlsConf)
		if err != nil {
			errC <- err
			close(errC)
			close(done)
			return
		}
		c, er := smtp.NewClient(con, strings.Split(m.Server, ":")[0])
		if er != nil {
			errC <- err
			close(errC)
			close(done)
			return
		}
		if err = c.Auth(auth); err != nil {
			errC <- err
			close(errC)
			close(done)
			return
		}
		if err = c.Mail(m.From); err != nil {
			errC <- err
			close(errC)
			close(done)
			return
		}
		if err = c.Rcpt(m.To); err != nil {
			errC <- err
			close(errC)
			close(done)
			return
		}
		w, err := c.Data()
		if err != nil {
			errC <- err
			close(errC)
			close(done)
			return
		}
		_, err = w.Write(m.PrepareMessage())
		if err != nil {
			errC <- err
			close(errC)
			close(done)
			return
		}
		err = w.Close()
		if err != nil {
			errC <- err
			close(errC)
			close(done)
			return
		}
		c.Quit()
		done <- true
		close(errC)
		close(done)
	}()
	return done, errC
}
